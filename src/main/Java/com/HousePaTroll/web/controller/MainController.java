package com.HousePaTroll.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Controller
public class MainController {

    private final Logger logger = LoggerFactory.getLogger("MainController");

    @GetMapping("/")
    public String index(Model model) {
        logger.debug("Welcome to HousePaTroll.com...");
        model.addAttribute("title", getTitle());
        model.addAttribute("msg", getMessage());
        model.addAttribute("today", new Date());
        return "index";
    }

    private String getMessage() {
        return "Hello World";
    }

    private String getTitle() {
        return getClass().getPackage().getSpecificationTitle();
    }

}
